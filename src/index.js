import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Import Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
//root.render (element)

// render() - displays the react elements/components int the root
root.render (
    <React.StrictMode>
        <App />
    </React.StrictMode>
)
