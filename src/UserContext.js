import React from 'react';

// Create a contect object
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;
